import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class WorkerDetails extends Details {

    private int noCores;
    private WorkerStatus workerStatus;
    private Work currentWork = null;

    public WorkerDetails(Socket s, ObjectInputStream dIn, ObjectOutputStream dOut, int noCores, WorkerStatus workerStatus) {
        this.setSocket(s);
        this.setdIn(dIn);
        this.setdOut(dOut);
        this.noCores = noCores;
        this.workerStatus = workerStatus;
    }

    public int getNoCores() {
        return noCores;
    }

    public void setNoCores(int noCores) {
        this.noCores = noCores;
    }

    public WorkerStatus getWorkerStatus() {
        return workerStatus;
    }

    public void setWorkerStatus(WorkerStatus workerStatus) {
        this.workerStatus = workerStatus;
    }

    public Work getCurrentWork() {
        return currentWork;
    }

    public void setCurrentWork(Work currentWork) {
        this.currentWork = currentWork;
    }
}
