import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClientDetails extends Details {
    public ClientDetails(Socket s, ObjectInputStream dIn, ObjectOutputStream dOut, long lastPingTime) {
        this.setSocket(s);
        this.setdIn(dIn);
        this.setdOut(dOut);
        this.setLastPingTime(lastPingTime);
    }
}
