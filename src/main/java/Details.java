import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public abstract class Details {
    private Socket s;
    private ObjectInputStream dIn;
    private ObjectOutputStream dOut;
    private long lastPingTime;

    public Socket getSocket() {
        return s;
    }

    public void setSocket(Socket s) {
        this.s = s;
    }

    public ObjectInputStream getdIn() {
        return dIn;
    }

    public void setdIn(ObjectInputStream dIn) {
        this.dIn = dIn;
    }

    public ObjectOutputStream getdOut() {
        return dOut;
    }

    public void setdOut(ObjectOutputStream dOut) {
        this.dOut = dOut;
    }

    public long getLastPingTime() {
        return lastPingTime;
    }

    public void setLastPingTime(long lastPingTime) {
        this.lastPingTime = lastPingTime;
    }
}
