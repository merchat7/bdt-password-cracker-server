import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.SocketTimeoutException;
import java.util.concurrent.ConcurrentLinkedQueue;

public class WorkerHandler implements Runnable {
    private String validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private char[] firstThreeChars = "   ".toCharArray();
    private String hash;

    public WorkerHandler(String hash) {
        this.hash = hash;
    }

    public void run() {
        Server.failedWorks = new ConcurrentLinkedQueue<Work>();
        for (int i = -1; i < validChars.length(); i++) {
            if (i != -1)  firstThreeChars[0] = validChars.charAt(i);
            for (int j = -1; j < validChars.length(); j++) {
                if (j != -1) firstThreeChars[1] = validChars.charAt(j);
                for (int k = -1; k < validChars.length(); k++) {
                    if (k != -1)  firstThreeChars[2] = validChars.charAt(k);
                    try {
                        checkCurrentRange();
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        }
    }

    private void checkCurrentRange() throws InterruptedException {
        int chunkSize = 8;
        int start = 0;
        int end = start + chunkSize;
        boolean toBreak = false;
        while (!toBreak){
            if (end >=  62) { // should be 62
                end = 62; // should be 62
                toBreak = true;
            }
            Work work = new Work(firstThreeChars.clone(), hash, start, end); // clone is required to fix a bug
            assignWork(work);
            // If there are any failed work, assign them first
            while (!Server.failedWorks.isEmpty()) assignWork(Server.failedWorks.poll());

            start += chunkSize;
            end += chunkSize;
        }
    }

    private void assignWork(Work work) throws InterruptedException {
        while (true) {
            System.out.println(String.format("Waiting for a free worker... [Start=%d, end=%d, (%s)]", work.getStart(), work.getEnd(), String.valueOf(work.getFirstThreeChars())));
            String workerName = Server.freeWorkers.take();
            System.out.println("Got worker " + workerName);
            WorkerDetails workerDetails = Server.workerDetails.get(workerName);
            if (workerDetails != null && workerDetails.getWorkerStatus() == WorkerStatus.AVAILABLE) {
                System.out.println("Sending work to worker...");
                if (handleRequest(workerDetails.getdIn(), workerDetails.getdOut(), work)) {
                    workerDetails.setCurrentWork(work);
                    workerDetails.setWorkerStatus(WorkerStatus.BUSY);
                    System.out.println("Successfully send work to worker");
                    break;
                }
                // Otherwise wait for another worker to assign the work to

            }
            else {
                System.out.println("Worker is dead or is busy");
            }
        }
    }

    private boolean handleRequest(ObjectInputStream dIn, ObjectOutputStream dOut, Work work) throws InterruptedException {
        try {
            int retry = 0;
            while (retry < 3) {
                if (!Message.assignWork(dOut, work)) {
                    retry += 1;
                    Thread.sleep(1000);
                    continue;
                } else retry = 0;

                if (Message.receiveAckWork(dIn)) return true;
                else {
                    // Request packet was somehow corrupted or other errors
                    retry += 1;
                    Thread.sleep(1000);
                    // continue;
                }
            }
            if (retry == 3) throw new SocketTimeoutException();
        } catch (SocketTimeoutException e) {
            System.out.println("Time out limited exceeded");
        }
        return false;
    }
}
