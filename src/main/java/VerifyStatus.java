import java.util.ArrayList;
import java.util.TimerTask;

public class VerifyStatus extends TimerTask {
    @Override
    public void run() {

        ClientDetails clientDetail = Server.getClientDetail();
        if (clientDetail != null) {
            handleTimeout("Client", "client", clientDetail.getLastPingTime());
        }

        for (String name : new ArrayList<String>(Server.workerDetails.keySet())) {
            long lastPingTime = Server.workerDetails.get(name).getLastPingTime();
            handleTimeout(name, "worker", lastPingTime);
        }
    }

    private void handleTimeout (String name, String type, long lastPingTime) {
        long currentTime = System.currentTimeMillis();
        if (currentTime - lastPingTime > 15000) {
            System.out.println(name + " timeout");
            if (type.equals("client")) {
                Server.setClientDetail(null);
                Worker.cancel();
            } else if (type.equals("worker")) {
                WorkerDetails workerDetail = Server.workerDetails.get(name);
                if (workerDetail.getWorkerStatus() == WorkerStatus.BUSY) Server.failedWorks.add(workerDetail.getCurrentWork());
                Server.workerDetails.remove(name);
            }
        }
    }
}
