import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.SocketTimeoutException;

public class Message {
    public static boolean ackRequest(ObjectOutputStream dOut, String request) {
        try {
            dOut.writeUTF("received");
            dOut.writeUTF(request);
            dOut.flush();
            return true;
        } catch (IOException e) {
            System.out.println("Error sending ack back to requester");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean ackResult(ObjectOutputStream dOut) {
        try {
            dOut.writeUTF("receivedResult");
            dOut.flush();
            return true;
        } catch (IOException e) {
            System.out.println("Error sending ack of result back to worker");
            e.printStackTrace();
            return false;
        }
    }

    public static void receivePing(ObjectInputStream dIn, String clientName) {
        try {
            String messageType = dIn.readUTF();
            long currentPingTime = System.currentTimeMillis();
            if (messageType.equals("ping")) {
                // clientName += "client";
                ClientDetails clientDetail = Server.getClientDetail();
                if (clientDetail != null) {
                    clientDetail.setLastPingTime(currentPingTime);
                }

            } else if (messageType.equals("pingWorker")) {
                clientName += "worker";
                WorkerDetails workerDetail = Server.workerDetails.get(clientName);
                if (workerDetail != null) {
                    workerDetail.setLastPingTime(currentPingTime);
                }
            }
        }
        catch (IOException e) {
            System.out.println("Failed to receive ping from client");
            e.printStackTrace();
        }
    }

    public static boolean assignWork(ObjectOutputStream dOut, Work work) {
        try {
            dOut.writeUTF("work");
            dOut.writeObject(work);
            dOut.flush();
            return true;
        } catch (IOException e) {
            System.out.println("Failed to assign work to worker");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean receiveAckWork(ObjectInputStream dIn) throws SocketTimeoutException {
        try {
            String messageType = dIn.readUTF();
            if (messageType.equals("receivedWork")) return true;
        } catch (SocketTimeoutException e) {
            // Timeout = 15 seconds, so should terminate!
            throw new SocketTimeoutException();
        } catch (IOException e) {
            System.out.println("Failed to receive ack for work from worker");
            e.printStackTrace();
        }
        return false;
    }

    public static void shutdown(ObjectOutputStream dOut) {
        try {
            dOut.writeUTF("shutdown");
            dOut.flush();
        } catch (IOException e) {
            System.out.println("Failed to shutdown worker");
            e.printStackTrace();
        }
    }

    public static void cancel(ObjectOutputStream dOut) {
        try {
            dOut.writeUTF("cancel");
            dOut.flush();
        } catch (IOException e) {
            System.out.println("Failed to cancel worker");
            e.printStackTrace();
        }
    }

    public static void sendResult(ObjectOutputStream dOut, String result) {
        try {
            dOut.writeUTF(result);
            dOut.flush();
        } catch (IOException e) {
            System.out.println("Failed to send result to client");
            e.printStackTrace();
        }
    }
}
