import java.io.Serializable;

public class Work implements Serializable {
    private char[] firstThreeChars;
    private String hash;
    private int start;
    private int end;

    public Work(char[] firstThreeChars, String hash, int start, int end) {
        this.firstThreeChars = firstThreeChars;
        this.hash = hash;
        this.start = start;
        this.end = end;
    }

    public char[] getFirstThreeChars() {
        return firstThreeChars;
    }

    public String getHash() {
        return hash;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }
}
