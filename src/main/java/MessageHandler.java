import java.io.*;
import java.net.Socket;
import java.util.concurrent.Executors;

public class MessageHandler implements Runnable {
    private Socket clientSocket;


    public MessageHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public void run() {
        String message;
        String clientName = clientSocket.getInetAddress().getHostAddress();
        try {
            clientSocket.setSoTimeout(15000); // 15 seconds
            ObjectOutputStream dOut = new ObjectOutputStream(clientSocket.getOutputStream());
            ObjectInputStream dIn = new ObjectInputStream(clientSocket.getInputStream());
            byte messageType = dIn.readByte();
            switch (messageType) {
                case 1: // Client related
                    if (Server.getClientDetail() == null) {
                        clientName += "client";
                        Server.setClientDetail(new ClientDetails(clientSocket, dIn, dOut, System.currentTimeMillis()));
                        message = dIn.readUTF();
                        System.out.println("Cracking request: " + message + " from " + clientName);
                        Message.ackRequest(dOut, message);
                        // Create a new thread to handle work for the request

                        if (Server.workerService != null) Worker.cancel(); // Ensure no workers is working on old jobs upon accepting new client
                        Server.workerService = Executors.newSingleThreadExecutor();
                        Server.workerService.submit(new WorkerHandler(message));
                    }
                    else {
                        System.out.println("Not currently accepting new client");
                        // TODO: Let client know its request wasn't accepted
                    }
                    break;
                case 2: // // Worker related
                    clientName += "worker";
                    message  = dIn.readUTF();
                    int noCores = Integer.parseInt(dIn.readUTF());
                    WorkerDetails workerDetails = new WorkerDetails(clientSocket, dIn, dOut, noCores, WorkerStatus.AVAILABLE);
                    Server.workerDetails.put(clientName, workerDetails);
                    Server.freeWorkers.put(clientName);

                    System.out.println("Worker join request from " + clientName);
                    Message.ackRequest(dOut, message);
                    break;
                case 3: // Ping related;
                    Message.receivePing(dIn, clientName);
                    break;
                case 4: // Worker's result
                    clientName += "worker";
                    String result = dIn.readUTF();
                    Message.ackResult(dOut);
                    if (!result.equals("Not found yet")) {
                        System.out.println("Request finished");
                        // Clean-up
                        Worker.cancel();
                        ClientDetails clientDetail = Server.getClientDetail();
                        if (clientDetail != null) {
                            Message.sendResult(clientDetail.getdOut(), result);
                            Server.setClientDetail(null);
                        }
                        else {
                            System.out.println("Client could not be found while trying to send result");
                        }
                    }
                    else {
                        Server.workerDetails.get(clientName).setWorkerStatus(WorkerStatus.AVAILABLE);
                        Server.freeWorkers.put(clientName);
                    }
                    break;
                case 5: // Worker terminated
                    clientName += "worker";
                    if (Server.workerDetails.get(clientName).getWorkerStatus() == WorkerStatus.AVAILABLE) Server.workerDetails.remove(clientName);
                    // Otherwise let VerifyStatus take care of it
                    break;
                default:
                    System.out.println("Message is not supported");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
