import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Queue;
import java.util.Timer;
import java.util.concurrent.*;

public class Server {
    private static ExecutorService messageService = Executors.newCachedThreadPool();
    static ExecutorService workerService = null;
    private static ClientDetails clientDetail = null;
    static Map<String, WorkerDetails> workerDetails = new ConcurrentHashMap<String, WorkerDetails>();
    static BlockingQueue<String> freeWorkers = new LinkedBlockingDeque<String>();
    static Queue<Work> failedWorks = new ConcurrentLinkedQueue<Work>();

    public synchronized static ClientDetails getClientDetail() {
        return clientDetail;
    }

    public synchronized static void setClientDetail(ClientDetails clientDetail) {
        Server.clientDetail = clientDetail;
    }

    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    Thread.sleep(200);
                    for (WorkerDetails workerDetails : workerDetails.values()) {
                        Message.shutdown(workerDetails.getdOut());
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            System.out.println("Server IP: " + InetAddress.getLocalHost().getHostAddress() + "@2165");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        // Timer to check if any client/worker timed out
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new VerifyStatus(), 0, 5000);
        try {
            ServerSocket serverSocket = new ServerSocket(2165);
            System.out.println("Server had started");
            while (true) {
                Socket clientSocket = serverSocket.accept();
                messageService.submit(new MessageHandler(clientSocket));
            }
        } catch (IOException e) {
            System.out.println("Couldn't create the server socket");
            e.printStackTrace();
            System.exit(1);
        }
    }
}
