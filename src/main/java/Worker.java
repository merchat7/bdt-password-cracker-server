public class Worker {
    // Terminate the worker service and any workers that are currently working
    public static void cancel () {
        Server.workerService.shutdownNow();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            return;
        }
        for (String name : Server.workerDetails.keySet()) {
            WorkerDetails workerDetail = Server.workerDetails.get(name);
            if (workerDetail.getWorkerStatus() == WorkerStatus.BUSY) {
                workerDetail.setWorkerStatus(WorkerStatus.AVAILABLE);
                try {
                    Server.freeWorkers.put(name);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Message.cancel(workerDetail.getdOut());
            }
        }
    }
}
